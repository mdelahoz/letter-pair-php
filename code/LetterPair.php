<?php
class LetterPair {

    protected $pairText;

    /**
     * @return mixed
     */
    public function getPairText()
    {
        return $this->pairText;
    }

    /**
     * @param mixed $pairText
     */
    public function setPairText($pairText)
    {
        $this->pairText = $pairText;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }
    protected $count;

    /**
     * Consrtuctor
     *
     * @param $pairText
     * @param $count
     */
    public function __construct($pairText, $count) {
        $this->pairText = $pairText;
        $this->count = $count;
    }
}
