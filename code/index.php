<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Code Challenge</title>
</head>
<body>
<div class="container">
    <div class="row">
        <h1>Code Challenge</h1>
        <form action="submit.php" method="post">
            <label for="text">Enter a string</label>
            <input type="text" name="text" placeholder="This is a single sample of text">
            <input type="submit" value="submit"  />
        </form>
    </div>
    <hr>
    <div class="row">
        <h2>Requirements</h2>
        <div>
            <p>Write a program to count the occurrences of all letter pairs in a sample of text. Disregard differences
                between lower and upper case letters.
                A letter pair is defined as a consecutive pair of letters that occur.</p>

            <p>You can write this in whatever language you prefer (Javascript is ideal for this role), but I should be
                able to easily run it after it is submitted. It should be robust.</p>

            <p>- Example input: `This is a single sample of text.`</p>


            <p>- Example output (could be a string or a data structure):</p>

            th->1

            hi->1

            is->2

            si->1

            in->1

            ng->1

            gl->1

            le->2

            sa->1

            am->1

            mp->1

            pl->1

            of->1

            te->1

            ex->1

            xt->1

        </div>
    </div>
</div>

</body>
</html>
