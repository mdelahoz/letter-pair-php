<?php
include ('LetterPair.php');
function isValidString($text) {
    if (strlen($text) > 0) {
        return true;
    }
    return false;
}

function isValidPair($char1, $char2) {
    if (preg_match('/[a-z]/', $char1) && preg_match('/[a-z]/', $char2)) {
        return $char1.$char2;
    }
    return false;
}


function getPairs($text) {
    $pairs = new SplObjectStorage();
    $pairsTmp = array();
    echo $text;
    if (isValidString($text)) {
        $lowerText = strtolower($text);
        // loop throgh string
        $textLength = strlen( $lowerText );
        for( $i = 0; $i <= $textLength; $i++ ) {
            $char1 = substr( $lowerText, $i, 1 );
            $char2 = substr( $lowerText, $i+1, 1 );

            $pair = isValidPair($char1, $char2);

            if ($pair) {
                if (count($pairsTmp) > 0) {
                    $isInPairs = in_array($pair, $pairsTmp);

                    if ($isInPairs) {
                        // increment count
                        $pairs->rewind();
                        while($pairs->valid()) {
                            $index  = $pairs->key();
                            $object = $pairs->current(); // similar to current($s)

                            if ($object->getPairText() === $pair){
                                $currentCount = $object->getCount();
                                $object->setCount($currentCount + 1);
                            }
                            $pairs->next();
                        }
                    } else {
                        array_push($pairsTmp, $pair);
                        $pairObj = new LetterPair($pair, 1);
                        $pairs->attach($pairObj);
                    }

                } else {
                    array_push($pairsTmp, $pair);
                    $pairObj = new LetterPair($pair, 1);
                    $pairs->attach($pairObj);
                }
            }

        }

        return $pairs;
    }
    return 'Not valid string';
}
