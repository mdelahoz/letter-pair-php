The Docker setup for PHP applications 

## Instructions
* Run `docker-compose up`
* Navigate to localhost:8100 Or if using windows home edition 192.168.99.100:8100


## Question: 

Write a program to count the occurrences of all letter pairs in a sample of text. Disregard differences between lower and upper case letters.
A letter pair is defined as a consecutive pair of letters that occur. 

You can write this in whatever language you prefer (Javascript is ideal for this role), but I should be able to easily run it after it is submitted. It should be robust.

- Example input: `This is a single sample of text.`


- Example output (could be a string or a data structure): 

th->1

hi->1

is->2

si->1

in->1

ng->1

gl->1

le->2

sa->1

am->1

mp->1

pl->1

of->1

te->1

ex->1

xt->1
